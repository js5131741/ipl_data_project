const numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch = require('../src/server/5-numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch.js');
const matches_data = [
  { toss_winner: 'RCB', winner: 'RCB' },
  { toss_winner: 'CSK', winner: 'RCB' },
  { toss_winner: 'CSK', winner: 'RCB' },
  { toss_winner: 'CSK', winner: 'RCB' },
  { toss_winner: 'CSK', winner: 'RCB' },
  { toss_winner: 'CSK', winner: 'RCB' },
];

const expected_result = {
  RCB: 1,
};

test('Correct', () => {
  expect(
    numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch(matches_data),
  ).toEqual(expected_result);
});
