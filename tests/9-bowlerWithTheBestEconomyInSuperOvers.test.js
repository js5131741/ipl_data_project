const bowlerWithTheBestEconomyInSuperOvers = require('../src/server/9-bowlerWithTheBestEconomyInSuperOvers.js');

const deliveries_data = [
  {
    is_super_over: 1,
    bowler: 'Ashwin',
    total_runs: 2,
    bye_runs: 0,
    legbye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 1,
  },
  {
    is_super_over: 1,
    bowler: 'Ashwin',
    total_runs: 2,
    bye_runs: 0,
    legbye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 2,
  },
  {
    is_super_over: 1,
    bowler: 'Ashwin',
    total_runs: 2,
    bye_runs: 0,
    legbye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 3,
  },
  {
    is_super_over: 1,
    bowler: 'Ashwin',
    total_runs: 2,
    bye_runs: 0,
    legbye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 4,
  },
  {
    is_super_over: 1,
    bowler: 'Ashwin',
    total_runs: 2,
    bye_runs: 0,
    legbye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 5,
  },
  {
    is_super_over: 0,
    bowler: 'Ashwin',
    total_runs: 3,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    legbye_runs: 0,
    ball: 1,
  },
];

const expected_result = {
  Ashwin: 12,
};

test('Correct', () => {
  expect(bowlerWithTheBestEconomyInSuperOvers(deliveries_data)).toEqual(
    expected_result,
  );
});
