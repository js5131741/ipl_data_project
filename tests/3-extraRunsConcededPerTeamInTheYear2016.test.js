const extraRunsConcededPerTeamInTheYear2016 = require('../src/server/3-extraRunsConcededPerTeamInTheYear2016.js');

const matches_data = [
  { season: 2016, id: 1 },
  { season: 2016, id: 2 },
  { season: 2016, id: 3 },
  { season: 2016, id: 4 },
  { season: 2016, id: 5 },
  { season: 2017, id: 10 },
];

const deliveries_data = [
  { match_id: 1, bowling_team: 'RCB', extra_runs: 1 },
  { match_id: 1, bowling_team: 'CSK', extra_runs: 2 },
  { match_id: 2, bowling_team: 'RCB', extra_runs: 1 },
  { match_id: 2, bowling_team: 'CSK', extra_runs: 2 },
  { match_id: 3, bowling_team: 'RCB', extra_runs: 1 },
  { match_id: 3, bowling_team: 'CSK', extra_runs: 2 },
  { match_id: 4, bowling_team: 'RCB', extra_runs: 1 },
  { match_id: 4, bowling_team: 'CSK', extra_runs: 2 },
  { match_id: 5, bowling_team: 'RCB', extra_runs: 1 },
  { match_id: 5, bowling_team: 'CSK', extra_runs: 2 },
  { match_id: 10, bowling_team: 'RCB', extra_runs: 1 },
];

const expected_result = {
  RCB: 5,
  CSK: 10,
};

test('Correct', () => {
  expect(
    extraRunsConcededPerTeamInTheYear2016(matches_data, deliveries_data),
  ).toEqual(expected_result);
});
