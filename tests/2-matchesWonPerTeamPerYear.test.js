const matchesWonPerTeamPerYear = require('../src/server/2-matchesWonPerTeamPerYear.js');

const data = [
  { season: 2018, winner: 'A' },
  { season: 2017, winner: 'A' },
  { season: 2016, winner: 'B' },
  { season: 2016, winner: 'B' },
  { season: 2017, winner: 'A' },
  { season: 2018, winner: 'B' },
];

const expected_result = {
  2016: { B: 2 },
  2017: { A: 2 },
  2018: { A: 1, B: 1 },
};

test('Correct', () => {
  expect(matchesWonPerTeamPerYear(data)).toEqual(expected_result);
});
