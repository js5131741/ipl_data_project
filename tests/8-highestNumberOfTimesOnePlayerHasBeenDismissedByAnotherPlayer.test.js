const highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer = require('../src/server/8-highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer.js');

const deliveries_data = [
  {
    id: 1,
    player_dismissed: 'Ashwin',
    bowler: 'Hardik',
    dismissal_kind: 'bowled',
  },
  {
    id: 4,
    player_dismissed: 'Ashwin',
    bowler: 'Hardik',
    dismissal_kind: 'bowled',
  },
  {
    id: 2,
    player_dismissed: 'Kohli',
    bowler: 'Hardik',
    dismissal_kind: 'bowled',
  },
  {
    id: 5,
    player_dismissed: 'Kohli',
    bowler: 'Hardik',
    dismissal_kind: 'bowled',
  },
  {
    id: 6,
    player_dismissed: 'Kohli',
    bowler: 'Hardik',
    dismissal_kind: 'run out',
  },
  {
    id: 3,
    player_dismissed: 'Kohli',
    bowler: 'Hardik',
    dismissal_kind: 'bowled',
  },
];

const expected_result = {
  Kohli: { Hardik: 3 },
};

test('Correct', () => {
  expect(
    highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer(
      deliveries_data,
    ),
  ).toEqual(expected_result);
});
