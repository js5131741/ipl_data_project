const playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason = require('../src/server/6-playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason.js');
const matches_data = [
  { season: 2015, player_of_match: 'Ashwin' },
  { season: 2015, player_of_match: 'Ashwin' },
  { season: 2015, player_of_match: 'Hardik' },
  { season: 2016, player_of_match: 'Hardik' },
  { season: 2016, player_of_match: 'Hardik' },
  { season: 2016, player_of_match: 'Ashwin' },
];

const expected_result = {
  2015: { Ashwin: 2 },
  2016: { Hardik: 2 },
};

test('Correct', () => {
  expect(
    playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason(
      matches_data,
    ),
  ).toEqual(expected_result);
});
