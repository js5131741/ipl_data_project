const matchesPerYear = require('../src/server/1-matchesPerYear.js');

const data = [
  { season: 2018 },
  { season: 2017 },
  { season: 2016 },
  { season: 2016 },
  { season: 2017 },
  { season: 2019 },
];

test('Correct', () => {
  expect(matchesPerYear(data)).toEqual({ 2016: 2, 2017: 2, 2018: 1, 2019: 1 });
});
