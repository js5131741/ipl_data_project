const strikeRateOfABatsmanForEachSeason = require('../src/server/7-strikeRateOfABatsmanForEachSeason.js');

const deliveries_data = [
  {
    match_id: 1,
    batsman: 'V Kohli',
    batsman_runs: 2,
    wide_runs: 0,
    noball_runs: 0,
  },
  {
    match_id: 2,
    batsman: 'V Kohli',
    batsman_runs: 2,
    wide_runs: 0,
    noball_runs: 0,
  },
  {
    match_id: 3,
    batsman: 'V Kohli',
    batsman_runs: 2,
    wide_runs: 0,
    noball_runs: 0,
  },
  {
    match_id: 4,
    batsman: 'V Kohli',
    batsman_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
  },
  {
    match_id: 5,
    batsman: 'V Kohli',
    batsman_runs: 0,
    wide_runs: 1,
    noball_runs: 0,
  },
  {
    match_id: 6,
    batsman: 'V Kohli',
    batsman_runs: 3,
    wide_runs: 0,
    noball_runs: 0,
  },
];

const matches_data = [
  { season: 2015, id: 1 },
  { season: 2015, id: 2 },
  { season: 2015, id: 3 },
  { season: 2016, id: 4 },
  { season: 2016, id: 5 },
  { season: 2016, id: 6 },
];

const expected_result = {
  2015: '200.00',
  2016: '150.00',
};

test('Correct', () => {
  expect(
    strikeRateOfABatsmanForEachSeason(matches_data, deliveries_data),
  ).toEqual(expected_result);
});
