/* eslint-disable no-undef */
const top10EconomicalBowlersInTheYear2015 = require('../src/server/4.top10EconomicalBowlersInTheYear2015.js');

const matches_data = [
  { season: 2015, id: 1 },
  { season: 2015, id: 2 },
  { season: 2015, id: 3 },
  { season: 2015, id: 4 },
  { season: 2015, id: 5 },
  { season: 2018, id: 10 },
];

const deliveries_data = [
  {
    match_id: 1,
    bowler: 'Ashwin',
    total_runs: 1,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 1,
  },
  {
    match_id: 1,
    bowler: 'Hardik',
    total_runs: 2,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 1,
  },
  {
    match_id: 2,
    bowler: 'Hardik',
    total_runs: 1,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 2,
  },
  {
    match_id: 2,
    bowler: 'Hardik',
    total_runs: 2,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 3,
  },
  {
    match_id: 3,
    bowler: 'Hardik',
    total_runs: 1,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 4,
  },
  {
    match_id: 3,
    bowler: 'Ashwin',
    total_runs: 2,
    legbye_runs: 0,
    bye_runs: 0,
    ball: 2,
    wide_runs: 0,
    noball_runs: 0,
  },
  {
    match_id: 4,
    bowler: 'Hardik',
    total_runs: 1,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 5,
  },
  {
    match_id: 4,
    bowler: 'Ashwin',
    total_runs: 2,
    legbye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    bye_runs: 0,
    ball: 3,
  },
  {
    match_id: 5,
    bowler: 'Hardik',
    total_runs: 1,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 6,
  },
  {
    match_id: 5,
    bowler: 'Hardik',
    total_runs: 2,
    legbye_runs: 1,
    bye_runs: 0,
    wide_runs: 1,
    noball_runs: 0,
    ball: 7,
  },
  {
    match_id: 10,
    bowler: 'Ashwin',
    total_runs: 1,
    legbye_runs: 0,
    bye_runs: 0,
    wide_runs: 0,
    noball_runs: 0,
    ball: 7,
  },
];

let expected_result = {
  Hardik: '9.00',
  Ashwin: '10.00',
};

test('Correct', () => {
  expect(
    top10EconomicalBowlersInTheYear2015(matches_data, deliveries_data),
  ).toEqual(expected_result);
});
