/* eslint-disable no-undef */
const fs = require('fs');
const csv = require('csv-parser');
const matchesPerYear = require('./src/server/1-matchesPerYear.js');
const matchesWonPerTeamPerYear = require('./src/server/2-matchesWonPerTeamPerYear.js');
const extraRunsConcededPerTeamInTheYear2016 = require('./src/server/3-extraRunsConcededPerTeamInTheYear2016.js');
const top10EconomicalBowlersInTheYear2015 = require('./src/server/4.top10EconomicalBowlersInTheYear2015.js');
const numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch = require('./src/server/5-numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch.js');
const playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason = require('./src/server/6-playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason.js');
const strikeRateOfABatsmanForEachSeason = require('./src/server/7-strikeRateOfABatsmanForEachSeason.js');
const highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer = require('./src/server/8-highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer.js');
const bowlerWithTheBestEconomyInSuperOvers = require('./src/server/9-bowlerWithTheBestEconomyInSuperOvers.js');

const filePathMatches = './src/data/matches.csv';
const filePathDeliveries = './src/data/deliveries.csv';

const matchesPerYearjsonFilePath =
  './src/public/output/1-matchesPerYearjson.json';
const matchesWonPerTeamPerYearjsonFilePath =
  './src/public/output/2-matchesWonPerTeamPerYear.json';
const extraRunsConcededPerTeamInTheYear2016jsonFilePath =
  './src/public/output/3-extraRunsConcededPerTeamInTheYear2016.json';
const top10EconomicalBowlersInTheYear2015jsonFilePath =
  './src/public/output/4-top10EconomicalBowlersInTheYear2015.json';
const numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatchjsonFilePath =
  './src/public/output/5-numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch.json';
const playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeasonjsonFilePath =
  './src/public/output/6-playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason.json';
const strikeRateOfABatsmanForEachSeasonjsonFilePath =
  './src/public/output/7-strikeRateOfABatsmanForEachSeason.json';
const highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayerjsonFilePath =
  './src/public/output/8-highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer.json';
const bowlerWithTheBestEconomyInSuperOversjsonFilePath =
  './src/public/output/9-bowlerWithTheBestEconomyInSuperOvers.json';

const deliveries = [];
const matches = [];

fs.createReadStream(filePathMatches)
  .pipe(csv())
  .on('data', (row) => {
    matches.push(row);
  })
  .on('end', () => {
    fs.createReadStream(filePathDeliveries)
      .pipe(csv())
      .on('data', (row) => {
        deliveries.push(row);
      })
      .on('end', () => {
        // 1. Number of matches played per year for all the years in IPL.
        const problem_1jsonData = JSON.stringify(
          matchesPerYear(matches),
          null,
          2,
        );
        fs.writeFile(
          matchesPerYearjsonFilePath,
          problem_1jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem1 done.');
            }
          },
        );

        // 2. Number of matches won per team per year in IPL.
        const problem_2jsonData = JSON.stringify(
          matchesWonPerTeamPerYear(matches),
          null,
          2,
        );
        fs.writeFile(
          matchesWonPerTeamPerYearjsonFilePath,
          problem_2jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem2 done.');
            }
          },
        );

        // 3. Extra runs conceded per team in the year 2016
        const problem_3jsonData = JSON.stringify(
          extraRunsConcededPerTeamInTheYear2016(matches, deliveries),
          null,
          2,
        );
        fs.writeFile(
          extraRunsConcededPerTeamInTheYear2016jsonFilePath,
          problem_3jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem3 done.');
            }
          },
        );

        // 4. Top 10 economical bowlers in the year 2015
        const problem_4jsonData = JSON.stringify(
          top10EconomicalBowlersInTheYear2015(matches, deliveries),
          null,
          2,
        );
        fs.writeFile(
          top10EconomicalBowlersInTheYear2015jsonFilePath,
          problem_4jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem4 done.');
            }
          },
        );

        // 5. Find the number of times each team won the toss and also won the match
        const problem_5jsonData = JSON.stringify(
          numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch(matches),
          null,
          2,
        );
        fs.writeFile(
          numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatchjsonFilePath,
          problem_5jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem5 done.');
            }
          },
        );

        // 6. Find a player who has won the highest number of Player of the Match awards for each season
        const problem_6jsonData = JSON.stringify(
          playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason(
            matches,
          ),
          null,
          2,
        );
        fs.writeFile(
          playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeasonjsonFilePath,
          problem_6jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem6 done.');
            }
          },
        );

        // 7. Find the strike rate of a batsman for each season
        const problem_7jsonData = JSON.stringify(
          strikeRateOfABatsmanForEachSeason(matches, deliveries),
          null,
          2,
        );
        fs.writeFile(
          strikeRateOfABatsmanForEachSeasonjsonFilePath,
          problem_7jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem7 done.');
            }
          },
        );

        // 8. Find the highest number of times one player has been dismissed by another player
        const problem_8jsonData = JSON.stringify(
          highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer(
            deliveries,
          ),
        );
        fs.writeFile(
          highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayerjsonFilePath,
          problem_8jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem8 done.');
            }
          },
        );

        // 9. Find the bowler with the best economy in super overs
        const problem_9jsonData = JSON.stringify(
          bowlerWithTheBestEconomyInSuperOvers(deliveries),
        );
        fs.writeFile(
          bowlerWithTheBestEconomyInSuperOversjsonFilePath,
          problem_9jsonData,
          function (err) {
            if (err) {
              console.error('Error:', err);
            } else {
              console.log('Problem9 done.');
            }
          },
        );
      });
  });
