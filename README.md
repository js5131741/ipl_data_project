# IPL Project

## Steps to setup and run the project

1. Clone the repository from GitHub

`git clone https://gitlab.com/js5131741/ipl_data_project.git`

2. Navigate to the project directory

`cd ipl_data_project`

3. Install npm packages

`npm install`

4. Run the index file

`node index.js`

5. Run jest to test

`npx jest`

6. To checkout the graphs, checkout the webapp branch.

`git checkout webapp`

7. Find the README.md file in that branch for the steps.