/* eslint-disable no-undef */
function strikeRateOfABatsmanForEachSeason(matches, deliveries) {
  // filtering the data based on the selected batsman
  const kohliDeliveries = deliveries.filter(
    (item) => item.batsman == 'V Kohli',
  );

  // creating an object to store match id as key and it's season as value
  const matchToSeason = {};
  matches.forEach((match) => {
    matchToSeason[match['id']] = match['season'];
  });

  // Store the runs and balls for each season
  const strikeRatesBySeason = kohliDeliveries.reduce((result, delivery) => {
    const season = matchToSeason[delivery.match_id];
    result[season] = result[season] || { scores: 0, balls: 0 };
    if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
      result[season].scores += Number(delivery.batsman_runs);
      result[season].balls += 1;
    }
    return result;
  }, {});

  // Calculate the strike rate for each season
  const result = Object.keys(strikeRatesBySeason).reduce(
    (finalResult, season) => {
      const scores = strikeRatesBySeason[season].scores;
      const balls = strikeRatesBySeason[season].balls;
      finalResult[season] = ((scores / balls) * 100).toFixed(2);
      return finalResult;
    },
    {},
  );

  return result;
}
module.exports = strikeRateOfABatsmanForEachSeason;
