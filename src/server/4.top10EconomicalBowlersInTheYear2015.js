/* eslint-disable no-undef */
function top10EconomicalBowlersInTheYear2015(matches, deliveries) {
  // Filter to include 2015 season match IDs
  const matchIds = new Set(
    matches
      .filter(function (match) {
        return match.season == 2015;
      })
      .map(function (match) {
        return match.id;
      }),
  );

  // Create an object to store bowler data, including runs conceded and balls bowled
  const economicalBowlers = {};

  for (let delivery of deliveries) {
    if (matchIds.has(delivery.match_id)) {
      const bowler = delivery.bowler;

      if (!economicalBowlers[bowler]) {
        economicalBowlers[bowler] = { runs: 0, balls: 0 };
      }

      economicalBowlers[bowler].runs += Number(
        delivery.total_runs - delivery.bye_runs - delivery.legbye_runs,
      );

      if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
        economicalBowlers[bowler].balls += 1;
      }
    }
  }

  // Calculate economy rate for each bowler
  for (let key in economicalBowlers) {
    const runs = economicalBowlers[key].runs;
    const balls = economicalBowlers[key].balls;
    economicalBowlers[key] = ((runs / balls) * 6).toFixed(2);
  }

  // Sort and include only the top 10 bowlers
  const sortedEconomicalBowlers = Object.fromEntries(
    Object.entries(economicalBowlers)
      .sort((a, b) => a[1] - b[1])
      .slice(0, 10),
  );

  return sortedEconomicalBowlers;
}

module.exports = top10EconomicalBowlersInTheYear2015;
