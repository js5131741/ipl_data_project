/* eslint-disable no-undef */
function bowlerWithTheBestEconomyInSuperOvers(deliveries) {
  const result = deliveries
    .filter((delivery) => delivery['is_super_over'] == 1)
    .reduce((bowlerData, delivery) => {
      const bowler = delivery['bowler'];
      const runs =
        delivery['total_runs'] - delivery['bye_runs'] - delivery['legbye_runs'];

      if (!bowlerData[bowler]) {
        bowlerData[bowler] = { runs: Number(runs), balls: 0 };
      } else {
        bowlerData[bowler].runs += Number(runs);
      }

      if (delivery.wide_runs == 0 && delivery.noball_runs == 0) {
        bowlerData[bowler].balls += 1;
      }

      return bowlerData;
    }, {});

  for (let bowler in result) {
    result[bowler] = Number(
      ((result[bowler].runs / result[bowler].balls) * 6).toFixed(2),
    );
  }

  const bestEconomicalBowler = Object.fromEntries(
    Object.entries(result)
      .sort((a, b) => a[1] - b[1])
      .slice(0, 1),
  );

  return bestEconomicalBowler;
}

module.exports = bowlerWithTheBestEconomyInSuperOvers;
