/* eslint-disable no-undef */
function highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer(
  deliveries,
) {
  const dismissedTypesToCheck = new Set([
    'run out',
    'retired hurt',
    'obstructing the field',
  ]);

  const dismissals = deliveries.reduce((result, delivery) => {
    const playerDismissed = delivery['player_dismissed'];
    const dismissalKind = delivery['dismissal_kind'];
    const bowler = delivery['bowler'];

    if (
      playerDismissed &&
      playerDismissed !== '' &&
      !dismissedTypesToCheck.has(dismissalKind)
    ) {
      result[playerDismissed] = result[playerDismissed] || {};

      if (result[playerDismissed][bowler]) {
        result[playerDismissed][bowler]++;
      } else {
        result[playerDismissed][bowler] = 1;
      }
    }

    return result;
  }, {});

  let maxDismissals = 0;
  let result = null;

  for (let player in dismissals) {
    for (let bowler in dismissals[player]) {
      if (dismissals[player][bowler] > maxDismissals) {
        maxDismissals = dismissals[player][bowler];
        result = { [player]: { [bowler]: maxDismissals } };
      }
    }
  }

  return result;
}

module.exports = highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer;
