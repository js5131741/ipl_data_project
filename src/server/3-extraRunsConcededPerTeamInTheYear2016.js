/* eslint-disable no-undef */
function extraRunsConcededPerTeamInTheYear2016(matches, deliveries) {
  // filter only include 2016 season and map the match ids
  const matchIds = new Set(
    matches
      .filter(function (match) {
        return match.season == 2016;
      })
      .map(function (match) {
        return match.id;
      }),
  );

  // calculate extra runs for each team
  const extraRuns = deliveries.reduce((accumulator, delivery) => {
    if (matchIds.has(delivery.match_id)) {
      const bowlingTeam = delivery.bowling_team;
      const runs = Number(delivery.extra_runs);

      accumulator[bowlingTeam] = (accumulator[bowlingTeam] || 0) + runs;
    }
    return accumulator;
  }, {});
  return extraRuns;
}

module.exports = extraRunsConcededPerTeamInTheYear2016;
