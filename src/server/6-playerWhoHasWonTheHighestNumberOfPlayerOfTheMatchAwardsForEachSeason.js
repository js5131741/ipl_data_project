/* eslint-disable no-undef */
function playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason(
  matches,
) {
  const playerOfTheMatch = {};

  for (let match of matches) {
    const season = match['season'];
    const player = match['player_of_match'];

    if (!playerOfTheMatch[season]) {
      playerOfTheMatch[season] = {};
    }

    playerOfTheMatch[season][player] =
      (playerOfTheMatch[season][player] || 0) + 1;
  }

  for (let season in playerOfTheMatch) {
    const seasonData = playerOfTheMatch[season];
    const maxAwards = Math.max(...Object.values(seasonData));
    const topPlayers = Object.keys(seasonData).filter(
      (player) => seasonData[player] === maxAwards,
    );
    playerOfTheMatch[season] = Object.fromEntries(
      topPlayers.map((player) => [player, maxAwards]),
    );
  }

  return playerOfTheMatch;
}

module.exports =
  playerWhoHasWonTheHighestNumberOfPlayerOfTheMatchAwardsForEachSeason;
