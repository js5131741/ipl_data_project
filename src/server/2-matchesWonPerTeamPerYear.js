/* eslint-disable no-undef */
function matchesWonPerTeamPerYear(matches) {
  const years = matches.reduce((accumulator, match) => {
    const year = match['season'];
    const winner = match['winner'];

    accumulator[year] = accumulator[year] || {};
    accumulator[year][winner] = (accumulator[year][winner] || 0) + 1;

    return accumulator;
  }, {});

  return years;
}

module.exports = matchesWonPerTeamPerYear;
