/* eslint-disable no-undef */
function matchesPerYear(matches) {
  const yearMatches = matches.reduce((accumulator, match) => {
    const year = match['season'];
    accumulator[year] = (accumulator[year] || 0) + 1;

    return accumulator;
  }, {});
  return yearMatches;
}

module.exports = matchesPerYear;
