/* eslint-disable no-undef */
function numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch(matches) {
  // Teams as keys, count the number of times they wont both toss and game as values
  const tossAndMatchWon = {};

  for (let match of matches) {
    const tossWinner = match['toss_winner'];
    const matchWinner = match['winner'];

    if (tossWinner === matchWinner) {
      tossAndMatchWon[tossWinner] = (tossAndMatchWon[tossWinner] || 0) + 1;
    }
  }

  return tossAndMatchWon;
}

module.exports = numberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch;
